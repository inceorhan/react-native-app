import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
} from 'react-native';
import AppRouter from './src/router'
import {Provider} from 'react-redux';
import store from './src/store/index'
console.disableYellowBox = true;


class App extends Component {

  render() {
    return (
      <Provider store= {store}>
        <SafeAreaView style={{ flex: 1 }}>
          <StatusBar style={{  backgroundColor: '#01577c' }}/>
          <AppRouter/>
        </SafeAreaView>
      </Provider>  
      )
  }
};

const styles = StyleSheet.create({
  
});

export default App;
