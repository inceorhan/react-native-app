import React, { Component } from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

import { Container, Header, Body, Text, Content } from 'native-base'


class Home extends Component {

  render() {
  return (
      <Container>
        <Header>
            <Body>
                <Text style={{ color: '#fff' }}>Pabeda App</Text>
            </Body>
        </Header>
        <Content>
            <View style={{ padding: 10 }}>
                <Text style={{ fontSize: 18 }}>Hoşgeldiniz!</Text>
            </View>
        </Content>   
      </Container>
    )
  }
};

const styles = StyleSheet.create({
  
});

export default Home;
