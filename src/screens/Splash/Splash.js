import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image
} from 'react-native';
import * as allHelpers from './../../helpers/helpers'
import { colors, dimensions } from './../../styles/Base'

class Splash extends Component {

  constructor(props) {
      super(props)
  }

  componentDidMount() {
    allHelpers.getUserLoginStatus().then(loginStatus => {
      if(loginStatus) {
        setTimeout(() => {this.props.navigation.navigate('Home')}, 2000)
      }else {
        setTimeout(() => {this.props.navigation.navigate('Login')}, 2000)
      }
    }) 
  }


  render() {
    return (
        <View style={{ flex:1, backgroundColor: colors.darkBlue, alignItems: 'center', justifyContent: 'center'}}>
            <View style={{ alignItems: 'center', justifyContent: 'center',width: 100, height: 100, backgroundColor: colors.white, borderRadius: 100 }}>
              <Image style={{ width: 75, height: 75 }} source={require('./../../assets/logos/logo-no-brand.png')} />
            </View>
        </View>
        )
  }
};

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: 'red',
      alignItems: 'center',
      justifyContent: 'center',
  },
  text: {
      color: '#fff'
  }
});

export default Splash;
