import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux' 
import { Container, Header, Body, Text, Content, Icon  } from 'native-base'
import RegisterForm from './../../components/RegisterForm'
import { colors, fonts} from './../../styles/Base'


class Register extends Component {

    constructor(props) {
        super(props)
    }
  
    componentDidMount() {
        console.log('Register screen!')
    }
  
    _navigateToRegister() {
        this.props.navigation.navigate('Register')
    }

  
    render() {
     return (
       <Container style = {styles.container}>
          <Content>
                <View style={{ flex:1 }}>
                <Text style = {styles.text}>Kayıt Ol</Text>
                <View style={{ flex:1, position: 'relative', alignItems: 'center', marginTop: 30, }}>
                  <View style={{ position: 'absolute',backgroundColor: colors.darkBlue, width: 75, height: 75, zIndex: 9999, justifyContent: 'center', alignItems: 'center', borderRadius: 50, top: 0 }}>
                      <Icon type="EvilIcons" name="user" style = {{ color : colors.white, position: 'absolute', fontSize: 75 }} />
                  </View>
                  <View style={{ position: 'relative', zIndex: 99, width: '100%', top: 10}}>
                    <RegisterForm navigation={this.props.navigation}/>
                  </View>
                </View>
                </View>
          </Content>
        </Container>
      )
    }
};

const styles = StyleSheet.create({
  container : {
    flex: 1,
    backgroundColor : colors.darkBlue,
  },
  text : {
    textAlign : fonts.center,
    color : colors.white,
    fontSize : 22,
    marginTop : 30,
    position: 'relative',
    top: 15
  },
  redirectButton: {
    height: 45,
    marginHorizontal: 45,
    borderRadius: 5,
    marginTop: 10
  }
  
});



function mapStateToProps(state) {
  return {
    
  }
}

function mapDispatchToProps(dispatch) {
  return {
   
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register)

