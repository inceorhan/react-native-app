import {StyleSheet, Dimensions} from 'react-native'

export const dimensions = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}
  
export const colors  = {
  newRed: '#A91A18',
  darkBlue:'#003f5c',
  newYellow: '#FBBE00',
  textColorBlack: '#333333',
  grayBackgroundColor: '#f5f5f5',
  white: '#ffffff',
  black: '#000000',
  placeHolderColor: '#c9c9c9',
  transparentColor: 'transparent',
  actionGreen: '#00c489',
  grayTextColor: '#8e8e8e',
  grayBg: '#e2e2e2',
  gray: '#F5F5F5'
}

export const padding = {
  sm: 10,
  md: 20,
  lg: 30,
  xl: 40
}

export const fonts = {
  sm: 12,
  md: 18,
  lg: 28,
  center : 'center',
}
