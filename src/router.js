import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { fromLeft, fromRight, fromBottom } from 'react-navigation-transitions';
import SplashScreen from './screens/Splash/Splash'
import LoginScreen from './screens/Login/Login'
import LoginFormScreen from './components/LoginForm'
import RegisterFormScreen from './components/RegisterForm'
import RegisterScreen from './screens/Register/Register'
import HomeScreen from './screens/Home/Home'

const handleCustomTransition = ({ scenes }) => {
    const prevScene = scenes[scenes.length - 2];
    const nextScene = scenes[scenes.length - 1];
  
    // Custom transitions go there
    if (prevScene
      && prevScene.route.routeName === 'Splash'
      && nextScene.route.routeName === 'Login') {
      return fromBottom();
    } else if (prevScene
      && prevScene.route.routeName === 'Splash'
      && nextScene.route.routeName === 'Login') {
      return fromRight();
    } else if (prevScene
        && prevScene.route.routeName === 'Login'
        && nextScene.route.routeName === 'Register') {
        return fromRight();
    }
    return fromBottom();
  }

const appNavigator = createStackNavigator({
    Splash: {
        screen: SplashScreen,
        navigationOptions: {
            header: null
        }
    },
    Login: {
        screen: LoginScreen,
        navigationOptions: {
            header: null
        }
    },
    Register: {
        screen: RegisterScreen,
        navigationOptions: {
            header: null
        }
    },
    Home: {
        screen: HomeScreen,
        navigationOptions: {
            header: null
        }
    },
    LoginForm: {
        screen: LoginFormScreen,
        navigationOptions: {
            header: null
        }
    },
    RegisterForm: {
        screen: RegisterFormScreen,
        navigationOptions: {
            header: null
        }
    },
},
    {
        initialRouteName: 'Splash',
        transitionConfig: (nav) => handleCustomTransition(nav)
    }
)

const router = createAppContainer(appNavigator)

export default router