import React , { Component } from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  TouchableOpacity,
  Alert,
  ToastAndroid
} from 'react-native';
import { connect } from 'react-redux' 
import * as apiRequests from './../api/apiRequests'
import * as allHelpers from './../helpers/helpers'
import { Field,reduxForm } from 'redux-form';
import { colors, fonts } from './../styles/Base'

const validate = values => {
  const error= {};
  error.email= '';
  error.name= '';
  if(values.email === undefined){
    error.email = 'E-posta gerekli'
  }
  if(values.password === undefined){
    error.password = 'Şifre gerekli'
  }
  if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(values.email)){
    error.email= 'Geçersiz e-posta adresi..';
  }
  return error;
};

const renderField = ({ label, keyboardType, meta:{ touched, error, warning}, input: { onChange, ...resInput}, placeholder, secureTextEntry, autoCapitalize }) => {
  return(
    <View>
      <Text style={{ fontWeight: 'bold', paddingBottom: 10 }}>{label}</Text>
      <TextInput keyboardType={keyboardType} onChangeText={onChange} {...resInput} style={styles.input} placeholder={placeholder} secureTextEntry={secureTextEntry} autoCapitalize = {autoCapitalize}></TextInput>
      <View>
      {touched && ((error && <Text>{error}</Text>) || (warning && <Text>{warning}</Text>))}
      </View>
    </View>
  )
}

class LoginFormComponent extends Component {
  
  _submitForm = values => {
    this.props.apiDoLogin(values).then(response => {
      if(response.error != null) {
        Alert.alert(
          '',
          'Kullanıcı adı veya şifre hatalı',
          [
            {},
            {},
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
        );
      }else {
        ToastAndroid.show('Giriş başarılı!', ToastAndroid.SHORT);
        allHelpers.storeUserLoginStatus().then(loginStatus => {
          if(loginStatus) {
            setTimeout(() => {this.props.navigation.navigate('Home')}, 2000)
          }
        })
        
      }
    })
  }
  render() {
    const {handleSubmit} = this.props
    return (
          <View style = {styles.container}>
            <View style={{ marginTop: 15 }}>
              <Field keyboardType="email-address" label="E-posta" component={renderField} name='email' placeholder="E-posta" autoCapitalize = 'none' />
            </View>
            <Field keyboardType="default" label="Şifre" component={renderField} name='password' placeholder="Şifre" secureTextEntry={true} />
            <TouchableOpacity style={styles.submitButton} activeOpacity={1} onPress={handleSubmit(this._submitForm)}>
              <Text style={{ color: '#fff',textAlign: 'center', lineHeight: 45 }}>Giriş Yap</Text>
            </TouchableOpacity>
          </View>
        )
  }
}

const styles = StyleSheet.create({
  container :{
    flex: 1,
    backgroundColor : colors.white,
    margin : 30,
    padding: 25,
    borderRadius : 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
  },
  text : {
    textAlign : fonts.center,
    fontFamily: 'Cochin',
  },
  input: {
    backgroundColor: '#fffdf9',
    marginBottom : 15,
    borderRadius : 5,
    borderWidth: 1,
    borderColor: colors.grayBg,
    borderStyle: 'solid',
    height: 45
  },
  submitButton: {
    backgroundColor: '#f45905',
    
    textAlign: 'center',
    borderRadius: 5,
    height: 45
  }
});


function mapStateToProps(state) {
  return {
    
  }
}

function mapDispatchToProps(dispatch) {
  return {
    apiDoLogin: (values) => dispatch(apiRequests.doLogin(values))
  }
}

LoginFormComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginFormComponent);

export default reduxForm({
  form: 'login',
  validate
})(LoginFormComponent);
