import AsyncStorage from '@react-native-community/async-storage'

export const storeUserLoginStatus = async () => {
    try {
        await AsyncStorage.setItem("userLoginStatus", '1') // store current user login status
        return true
    }catch(e) {
        console.log(e)
    }
}

export const getUserLoginStatus = async () => {
    const userLoginStatus = await AsyncStorage.getItem("userLoginStatus")
    return userLoginStatus
}