const LOGIN_URL = 'https://popup-smart-api.webbayi.net/api/Auth/customer-login'
const REGISTER_URL = 'https://popup-smart-api.webbayi.net/api/User/register'

export const doLogin = (values) => {
    return async (dispatch, getState) => {        
          return fetch(LOGIN_URL, {
            method: 'POST',
            body: JSON.stringify({ 'email': values.email,'password': values.password}),
            headers:{
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
            })
            .then(res => res.json())
            .then((response)=>{ 
                return response
            })
            .catch(function (error) {
               console.log(error)
            })
    }
}

export const doRegister = (values) => {
    return async (dispatch, getState) => {        
        return fetch(REGISTER_URL, {
          method: 'POST',
          body: JSON.stringify({ 'email': values.email,'password': values.password}),
          headers:{
              'Content-Type': 'application/json',
              'Accept': 'application/json'
          }
          })
          .then(res => res.json())
          .then((response)=>{ 
              return response
          })
          .catch(function (error) {
             console.log(error)
          })
  }
}
